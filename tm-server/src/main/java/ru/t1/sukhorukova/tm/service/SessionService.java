package ru.t1.sukhorukova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.repository.ISessionRepository;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.ISessionService;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.IdEmptyException;
import ru.t1.sukhorukova.tm.exception.field.IndexIncorrectException;
import ru.t1.sukhorukova.tm.exception.field.UserIdEmptyException;
import ru.t1.sukhorukova.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    protected final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public SessionDTO add(@Nullable final SessionDTO session) {
        if (session == null) throw new EntityNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.add(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return session;
    }

    @Nullable
    @Override
    public SessionDTO add(
            @Nullable final String userId,
            @Nullable final SessionDTO session
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new EntityNotFoundException();

        session.setUserId(userId);

        return add(session);
    }

    @NotNull
    @Override
    public Collection<SessionDTO> add(@Nullable final Collection<SessionDTO> sessions) {
        if (sessions == null) throw new ValueIsNullException();

        for (final SessionDTO session : sessions) {
            add(session);
        }

        return sessions;
    }

    @NotNull
    @Override
    public Collection<SessionDTO> set(@Nullable final Collection<SessionDTO> sessions) {
        if (sessions == null) throw new ValueIsNullException();

        removeAll();

        return add(sessions);
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);

        try {
            return repository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.findAllByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.findOneById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.findOneByIdByUserId(userId, id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<SessionDTO> sessions = findAll();
        return sessions.get(index);
    }

    @Nullable
    @Override
    public SessionDTO findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();

        @NotNull final List<SessionDTO> sessions = findAll(userId);
        return sessions.get(index);
    }

    @Override
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeAllByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO removeOne(@Nullable final SessionDTO session) {
        if (session == null) throw new EntityNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeOneById(session.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return session;
    }

    @Override
    public SessionDTO removeOne(
            @Nullable final String userId,
            @Nullable final SessionDTO session
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new EntityNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeOneByIdByUserId(userId, session.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return session;
    }

    @Nullable
    @Override
    public SessionDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable SessionDTO session = findOneById(id);
        if (session == null) return null;

        return removeOne(session);
    }

    @Nullable
    @Override
    public SessionDTO removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable SessionDTO session = findOneById(userId, id);
        if (session == null) return null;

        return removeOne(userId, session);
    }

    @Nullable
    @Override
    public SessionDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<SessionDTO> sessions = findAll();
        return removeOneById(sessions.get(index).getId());
    }

    @Nullable
    @Override
    public SessionDTO removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<SessionDTO> sessions = findAll(userId);
        return removeOneById(sessions.get(index).getId());
    }

    @Override
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.getSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return repository.getSizeByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return findOneById(userId, id) != null;
    }

}
