package ru.t1.sukhorukova.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;
import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    SqlSession getSqlConnection();

    @NotNull
    EntityManagerFactory factory();

}
