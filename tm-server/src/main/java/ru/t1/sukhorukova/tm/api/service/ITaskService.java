package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDTO add(@Nullable TaskDTO task);

    @Nullable
    TaskDTO add(
            @Nullable String userId,
            @Nullable TaskDTO task
    );

    @NotNull
    Collection<TaskDTO> add(@NotNull Collection<TaskDTO> tasks);

    @NotNull
    Collection<TaskDTO> set(@NotNull Collection<TaskDTO> tasks);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAll(final String userId, final TaskSort sort);

    @Nullable
    TaskDTO findOneById(@Nullable String id);

    @Nullable
    TaskDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    TaskDTO findOneByIndex(@Nullable Integer index);

    @Nullable
    TaskDTO findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    TaskDTO removeOne(@Nullable TaskDTO task);

    TaskDTO removeOne(
            @Nullable String userId,
            @Nullable TaskDTO task
    );

    @Nullable
    TaskDTO removeOneById(@Nullable String id);

    @Nullable
    TaskDTO removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    TaskDTO removeOneByIndex(@Nullable Integer index);

    @Nullable
    TaskDTO removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll();

    void removeAll(@Nullable String userId);

    int getSize();

    int getSize(@Nullable String userId);

    boolean existsById(String id);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    TaskDTO changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    void removeProjectById(
            @Nullable String userId,
            @Nullable String projectId
    );

}
