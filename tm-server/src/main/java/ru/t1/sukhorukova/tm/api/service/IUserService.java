package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserService extends IService<UserDTO> {

    @NotNull
    UserDTO add(@Nullable UserDTO user);

    @NotNull
    Collection<UserDTO> add(@NotNull Collection<UserDTO> users);

    @NotNull
    Collection<UserDTO> set(@NotNull Collection<UserDTO> users);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findOneByIndex(@Nullable Integer index);

    @NotNull
    UserDTO removeOne(@Nullable UserDTO user);

    @Nullable
    UserDTO removeOneById(@Nullable String id);

    @Nullable
    UserDTO removeOneByIndex(@Nullable Integer index);

    void removeAll();

    int getSize();

    boolean existsById(String id);

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeOneByLogin(@Nullable String login);

    @Nullable
    UserDTO removeOneByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    UserDTO lockOneByLogin(@Nullable String login);

    @NotNull
    UserDTO unlockOneByLogin(@Nullable String login);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

}
