package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.AbstractUserOwnerModelDTO;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnerModelDTO> extends IService<M> {

    @Nullable
    M add(
            @Nullable String userId,
            @Nullable M model
    );

    @NotNull
    List<M> findAll(@Nullable String userId);

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeOne(
            @Nullable String userId,
            @Nullable M model
    );

    @Nullable
    M removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll(@Nullable String userId);

    int getSize(@Nullable String userId);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

}
