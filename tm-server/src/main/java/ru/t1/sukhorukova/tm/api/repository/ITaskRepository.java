package ru.t1.sukhorukova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @Insert(
            "INSERT INTO TM_TASK (ID, NAME, DESCRIPTION, USER_ID, PROJECT_ID, STATUS, CREATED) " +
            "VALUES (#{id}, #{name}, #{description}, #{userId}, #{projectId}, #{status}, #{created})"
    )
    void add(@NotNull final TaskDTO task);

    @Select("SELECT * FROM TM_TASK")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    @Nullable List<TaskDTO> findAll();

    @Select("SELECT * FROM TM_TASK WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    @Nullable List<TaskDTO> findAllByUserId(@NotNull final String userId);

    @Select("SELECT * FROM TM_TASK WHERE USER_ID = #{userId} ORDER BY NAME")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    @Nullable List<TaskDTO> findAllSortByName(@NotNull final String userId);

    @Select("SELECT * FROM TM_TASK WHERE USER_ID = #{userId} ORDER BY STATUS")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    @Nullable List<TaskDTO> findAllSortByStatus(@NotNull final String userId);

    @Select("SELECT * FROM TM_TASK WHERE USER_ID = #{userId} ORDER BY CREATED")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    @Nullable List<TaskDTO> findAllSortByCreated(@NotNull final String userId);

    @Select("SELECT * FROM TM_TASK WHERE PROJECT_ID = #{projectId} AND USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    @Nullable List<TaskDTO> findAllByProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId
    );

    @Select("SELECT * FROM TM_TASK WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    @Nullable TaskDTO findOneById(@NotNull final String id);

    @Select("SELECT * FROM TM_TASK WHERE ID = #{id} AND USER_ID = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    @Nullable TaskDTO findOneByIdByUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id
    );

    @Delete("DELETE FROM TM_TASK WHERE ID = #{id}")
    void removeOneById(@NotNull String id);

    @Delete("DELETE FROM TM_TASK WHERE ID = #{id} AND USER_ID = #{userId}")
    void removeOneByIdByUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id
    );

    @Delete("DELETE FROM TM_TASK")
    void removeAll();

    @Delete("DELETE FROM TM_TASK WHERE USER_ID = #{userId}")
    void removeAllByUserId(@NotNull String userId);

    @Delete("DELETE FROM TM_TASK WHERE PROJECT_ID = #{projectId} AND USER_ID = #{userId}")
    void removeTasksByProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId
    );

    @Select("SELECT COUNT(1) FROM TM_TASK")
    int getSize();

    @Select("SELECT COUNT(1) FROM TM_TASK WHERE USER_ID = #{userId}")
    int getSizeByUserId(@NotNull String userId);

    @Update(
            "UPDATE TM_TASK" +
            "   SET NAME = #{name}," +
            "       DESCRIPTION = #{description}" +
            " WHERE ID = #{id} AND USER_ID = #{userId}"
    )
    void updateById(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id,
            @NotNull @Param("name") final String name,
            @NotNull @Param("description") final String description
    );

    @Update(
            "UPDATE TM_TASK" +
            "   SET STATUS = #{status}" +
            " WHERE ID = #{id} AND USER_ID = #{userId}"
    )
    void changeTaskStatusById(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id,
            @NotNull @Param("status") final Status status
    );

    @Update(
            "UPDATE TM_TASK" +
            "   SET PROJECT_ID = #{projectId}" +
            " WHERE ID = #{taskId} AND USER_ID = #{userId}"
    )
    void bindTaskToProject(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("taskId") final String taskId,
            @NotNull @Param("projectId") final String projectId
    );

    @Update(
            "UPDATE TM_TASK" +
            "   SET PROJECT_ID = NULL" +
            " WHERE ID = #{taskId} AND PROJECT_ID = #{projectId} AND USER_ID = #{userId}"
    )
    void unbindTaskFromProject(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("taskId") final String taskId,
            @NotNull @Param("projectId") final String projectId
    );

}
