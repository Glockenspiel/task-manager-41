package ru.t1.sukhorukova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert(
            "INSERT INTO TM_USER (ID, LOGIN, PASSWORD_HASH, ROLE, LOCKED, EMAIL, FIRST_NAME, LAST_NAME, MIDDLE_NAME) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{role}, #{locked}, #{email}, #{firstName}, #{lastName}, #{middleName})"
    )
    void add(@NotNull UserDTO user);

    @Select("SELECT * FROM TM_USER")
    @Results(value = {
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME")
    })
    @Nullable List<UserDTO> findAll();

    @Select("SELECT * FROM TM_USER WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME")
    })
    @Nullable UserDTO findOneById(@NotNull String id);

    @Select("SELECT * FROM TM_USER WHERE LOGIN = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME")
    })
    @Nullable UserDTO findByLogin(@NotNull String login);

    @Select("SELECT * FROM TM_USER WHERE EMAIL = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME")
    })
    @Nullable UserDTO findByEmail(@NotNull String email);

    @Delete("DELETE FROM TM_USER WHERE ID = #{id}")
    void removeOneById(@NotNull String id);

    @Delete("DELETE FROM TM_USER")
    void removeAll();

    @Select("SELECT COUNT(1) FROM TM_USER")
    int getSize();

    @Update(
            "UPDATE TM_USER" +
            "   SET PASSWORD_HASH = #{passwordHash}" +
            " WHERE ID = #{id}"
    )
    void setPasswordHashById(
            final @NotNull @Param("id") String id,
            final @NotNull @Param("passwordHash") String passwordHash
    );

    @Update(
            "UPDATE TM_USER" +
            "   SET FIRST_NAME = #{firstName}," +
            "       LAST_NAME = #{lastName}," +
            "       MIDDLE_NAME = #{middleName}" +
            " WHERE ID = #{id}"
    )
    void update(@NotNull final UserDTO user);

    @Update(
            "UPDATE TM_USER" +
            "   SET LOCKED = #{locked}" +
            " WHERE ID = #{id}"
    )
    void setLockById(
            @NotNull @Param("id") final String id,
            @NotNull @Param("locked") final Boolean locked
    );

}
