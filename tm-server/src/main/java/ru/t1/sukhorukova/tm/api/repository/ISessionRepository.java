package ru.t1.sukhorukova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Insert(
            "INSERT INTO TM_SESSION (ID, USER_ID, ROLE) " +
            "VALUES (#{id}, #{userId}, #{role})"
    )
    void add(@NotNull final SessionDTO session);

    @Select("SELECT * FROM TM_SESSION")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable List<SessionDTO> findAll();

    @Select("SELECT * FROM TM_SESSION WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable List<SessionDTO> findAllByUserId(@NotNull final String userId);

    @Select("SELECT * FROM TM_SESSION WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable SessionDTO findOneById(@NotNull final String id);

    @Select("SELECT * FROM TM_SESSION WHERE ID = #{id} AND USER_ID = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable SessionDTO findOneByIdByUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id
    );

    @Delete("DELETE FROM TM_SESSION WHERE ID = #{id}")
    void removeOneById(@NotNull final String id);

    @Delete("DELETE FROM TM_SESSION WHERE ID = #{id} AND USER_ID = #{userId}")
    void removeOneByIdByUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id
    );

    @Delete("DELETE FROM TM_SESSION")
    void removeAll();

    @Delete("DELETE FROM TM_SESSION WHERE USER_ID = #{userId}")
    void removeAllByUserId(@NotNull final String userId);

    @Select("SELECT COUNT(*) FROM TM_SESSION")
    int getSize();

    @Select("SELECT COUNT(*) FROM TM_SESSION WHERE USER_ID = #{userId}")
    int getSizeByUserId(@NotNull final String userId);

}
