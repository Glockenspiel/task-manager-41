package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractModelDTO> {

    @NotNull
    M add(@Nullable M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    @NotNull
    M removeOne(@Nullable M model);

    @Nullable
    M removeOneById(@Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable Integer index);

    void removeAll();

    int getSize();

    boolean existsById(String id);

}
