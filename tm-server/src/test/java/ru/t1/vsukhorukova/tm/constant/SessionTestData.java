package ru.t1.vsukhorukova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.SessionDTO;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.ADMIN;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static SessionDTO USER1_SESSION1 = new SessionDTO(USER1);

    @NotNull
    public final static SessionDTO USER1_SESSION2 = new SessionDTO(USER1);

    @NotNull
    public final static SessionDTO USER1_SESSION3 = new SessionDTO(USER1);

    @NotNull
    public final static SessionDTO USER2_SESSION1 = new SessionDTO(USER2);

    @NotNull
    public final static SessionDTO ADMIN_SESSION1 = new SessionDTO(ADMIN);

    @NotNull
    public final static SessionDTO ADMIN_SESSION2 = new SessionDTO(ADMIN);

    @NotNull
    public final static List<SessionDTO> USER1_SESSION_LIST = Arrays.asList(USER1_SESSION1, USER1_SESSION2, USER1_SESSION3);

    @NotNull
    public final static List<SessionDTO> USER2_SESSION_LIST = Collections.singletonList(USER2_SESSION1);

    @NotNull
    public final static List<SessionDTO> ADMIN_SESSION_LIST = Arrays.asList(ADMIN_SESSION1, ADMIN_SESSION2);

    @Nullable
    public final static SessionDTO NULL_SESSION = null;

    @Nullable
    public final static String NULL_SESSION_ID = null;

    @Nullable
    public final static List<SessionDTO> NULL_SESSION_LIST = null;

}
