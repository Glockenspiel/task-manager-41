package ru.t1.vsukhorukova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.dto.model.ProjectDTO;

import java.util.*;

import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static ProjectDTO USER1_PROJECT1 = new ProjectDTO(USER1, "PROJECT_01_01", "Test project 1 for user 1.", Status.COMPLETED);

    @NotNull
    public final static ProjectDTO USER1_PROJECT2 = new ProjectDTO(USER1, "PROJECT_01_02", "Test project 2 for user 1.", Status.IN_PROGRESS);

    @NotNull
    public final static ProjectDTO USER1_PROJECT3 = new ProjectDTO(USER1, "PROJECT_01_03", "Test project 3 for user 1.", Status.NOT_STARTED);

    @NotNull
    public final static ProjectDTO USER2_PROJECT1 = new ProjectDTO(USER2, "PROJECT_02_01", "Test project 1 for user 2.", Status.NOT_STARTED);

    @NotNull
    public final static ProjectDTO ADMIN_PROJECT1 = new ProjectDTO(ADMIN, "PROJECT_02_02", "Test project 2 for user 2.", Status.COMPLETED);

    @NotNull
    public final static ProjectDTO ADMIN_PROJECT2 = new ProjectDTO(ADMIN, "PROJECT_03_01", "Test project 1 for admin.", Status.NOT_STARTED);

    @NotNull
    public final static List<ProjectDTO> USER1_PROJECT_LIST = Arrays.asList(USER1_PROJECT1, USER1_PROJECT2, USER1_PROJECT3);

    @NotNull
    public final static List<ProjectDTO> USER2_PROJECT_LIST = Collections.singletonList(USER2_PROJECT1);

    @NotNull
    public final static List<ProjectDTO> ADMIN_PROJECT_LIST = Arrays.asList(ADMIN_PROJECT1, ADMIN_PROJECT2);

    @Nullable
    public final static ProjectDTO NULL_PROJECT = null;

    @Nullable
    public final static String NULL_PROJECT_ID = null;

    @Nullable
    public final static List<ProjectDTO> NULL_PROJECT_LIST = null;

}
