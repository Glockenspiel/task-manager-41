package ru.t1.vsukhorukova.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.api.service.ISessionService;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.IdEmptyException;
import ru.t1.sukhorukova.tm.exception.field.IndexIncorrectException;
import ru.t1.sukhorukova.tm.exception.field.UserIdEmptyException;
import ru.t1.sukhorukova.tm.dto.model.SessionDTO;
import ru.t1.sukhorukova.tm.service.ConnectionService;
import ru.t1.sukhorukova.tm.service.PropertyService;
import ru.t1.sukhorukova.tm.service.SessionService;
import ru.t1.vsukhorukova.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.SessionTestData.*;
import static ru.t1.vsukhorukova.tm.constant.SessionTestData.USER1_SESSION1;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void check() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @After
    public void clear() {
        sessionService.removeAll();
    }

    @Test
    public void testSessionAdd() {
        Assert.assertNotNull(sessionService.add(USER1_SESSION1));
        Assert.assertEquals(USER1_SESSION1.getId(), sessionService.findAll().get(0).getId());

        thrown.expect(EntityNotFoundException.class);
        sessionService.add(NULL_SESSION);
    }

    @Test
    public void testSessionAddByUserId() {
        Assert.assertNotNull(sessionService.add(USER1.getId(), USER1_SESSION1));
        Assert.assertEquals(USER1_SESSION1.getId(), sessionService.findAll().get(0).getId());

        thrown.expect(UserIdEmptyException.class);
        sessionService.add(NULL_USER_ID, USER1_SESSION1);
        thrown.expect(EntityNotFoundException.class);
        sessionService.add(USER1.getId(), NULL_SESSION);
    }

    @Test
    public void testSessionAddModels() {
        Assert.assertNotNull(sessionService.add(ADMIN_SESSION_LIST));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0).getId(), sessionService.findAll().get(0).getId());
        Assert.assertEquals(ADMIN_SESSION_LIST.get(1).getId(), sessionService.findAll().get(1).getId());

        thrown.expect(ValueIsNullException.class);
        sessionService.add(NULL_SESSION_LIST);
    }

    @Test
    public void testSessionSet() {
        Assert.assertNotNull(sessionService.set(ADMIN_SESSION_LIST));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0).getId(), sessionService.findAll().get(0).getId());
        Assert.assertEquals(ADMIN_SESSION_LIST.get(1).getId(), sessionService.findAll().get(1).getId());
        Assert.assertNotNull(sessionService.set(USER2_SESSION_LIST));
        Assert.assertEquals(USER2_SESSION_LIST.get(0).getId(), sessionService.findAll().get(0).getId());

        thrown.expect(ValueIsNullException.class);
        sessionService.add(NULL_SESSION_LIST);
    }

    @Test
    public void testSessionFindAll() {
        sessionService.add(USER1_SESSION_LIST);
        @NotNull final List<SessionDTO> sessionList = sessionService.findAll();
        Assert.assertEquals(USER1_SESSION_LIST.get(0).getId(), sessionList.get(0).getId());
        Assert.assertEquals(USER1_SESSION_LIST.get(1).getId(), sessionList.get(1).getId());
        Assert.assertEquals(USER1_SESSION_LIST.get(2).getId(), sessionList.get(2).getId());
    }

    @Test
    public void testSessionFindAllByUserId() {
        sessionService.add(USER1_SESSION_LIST);
        sessionService.add(ADMIN_SESSION_LIST);
        @NotNull final List<SessionDTO> sessionListUser1 = sessionService.findAll(USER1.getId());
        Assert.assertEquals(USER1_SESSION_LIST.get(0).getId(), sessionListUser1.get(0).getId());
        Assert.assertEquals(USER1_SESSION_LIST.get(1).getId(), sessionListUser1.get(1).getId());
        Assert.assertEquals(USER1_SESSION_LIST.get(2).getId(), sessionListUser1.get(2).getId());
        @NotNull final List<SessionDTO> sessionListAdmin = sessionService.findAll(ADMIN.getId());
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0).getId(), sessionListAdmin.get(0).getId());
        Assert.assertEquals(ADMIN_SESSION_LIST.get(1).getId(), sessionListAdmin.get(1).getId());

        thrown.expect(UserIdEmptyException.class);
        sessionService.findAll(NULL_USER_ID);
    }

    @Test
    public void testSessionFindOneById() {
        sessionService.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION2.getId(), sessionService.findOneById(USER1_SESSION2.getId()).getId());
        Assert.assertNull(sessionService.findOneById("test-id"));

        thrown.expect(IdEmptyException.class);
        sessionService.findOneById(NULL_SESSION_ID);
    }

    @Test
    public void testSessionFindOneByIdByUserId() {
        sessionService.add(USER1_SESSION_LIST);
        sessionService.add(ADMIN_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION2.getId(), sessionService.findOneById(USER1.getId(), USER1_SESSION2.getId()).getId());
        Assert.assertEquals(ADMIN_SESSION2.getId(), sessionService.findOneById(ADMIN.getId(), ADMIN_SESSION2.getId()).getId());
        Assert.assertNull(sessionService.findOneById(USER1.getId(), ADMIN_SESSION2.getId()));
        Assert.assertNull(sessionService.findOneById(USER1.getId(), "test-id"));

        thrown.expect(UserIdEmptyException.class);
        sessionService.findOneById(NULL_USER_ID, ADMIN_SESSION2.getId());
        thrown.expect(IdEmptyException.class);
        sessionService.findOneById(NULL_SESSION_ID);
    }

    @Test
    public void testSessionFindOneByIndex() {
        sessionService.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION2.getId(), sessionService.findOneByIndex(1).getId());

        thrown.expect(IndexIncorrectException.class);
        sessionService.findOneByIndex(-1);
    }

    @Test
    public void testSessionFindOneByIndexByUserId() {
        sessionService.add(USER1_SESSION_LIST);
        sessionService.add(ADMIN_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.get(1).getId(), sessionService.findOneByIndex(USER1.getId(), 1).getId());
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0).getId(), sessionService.findOneByIndex(ADMIN.getId(), 0).getId());

        thrown.expect(UserIdEmptyException.class);
        sessionService.findOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        sessionService.findOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testSessionRemoveOne() {
        sessionService.add(USER1_SESSION_LIST);
        Assert.assertNotNull(sessionService.findOneById(USER1_SESSION2.getId()));
        Assert.assertNotNull(sessionService.removeOne(USER1_SESSION2));
        Assert.assertNull(sessionService.findOneById(USER1_SESSION2.getId()));

        thrown.expect(EntityNotFoundException.class);
        sessionService.removeOne(NULL_SESSION);
    }

    @Test
    public void testSessionRemoveOneByUserId() {
        sessionService.add(USER1_SESSION_LIST);
        sessionService.add(USER2_SESSION_LIST);
        Assert.assertNotNull(sessionService.findOneById(USER1_SESSION2.getId()));
        sessionService.removeOne(USER2.getId(), USER1_SESSION2);
        Assert.assertNotNull(sessionService.findOneById(USER1_SESSION2.getId()));
        sessionService.removeOne(USER1.getId(), USER1_SESSION2);
        Assert.assertNull(sessionService.findOneById(USER1_SESSION2.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeOne(NULL_USER_ID, USER1_SESSION2);
        thrown.expect(EntityNotFoundException.class);
        sessionService.removeOne(USER1.getId(), NULL_SESSION);
    }

    @Test
    public void testSessionRemoveOneById() {
        sessionService.add(USER1_SESSION_LIST);
        Assert.assertNotNull(sessionService.findOneById(USER1_SESSION2.getId()));
        Assert.assertNotNull(sessionService.removeOneById(USER1_SESSION2.getId()));
        Assert.assertNull(sessionService.findOneById(USER1_SESSION2.getId()));

        thrown.expect(IdEmptyException.class);
        sessionService.removeOneById(NULL_SESSION_ID);
    }

    @Test
    public void testSessionRemoveOneByIdByUserId() {
        sessionService.add(USER1_SESSION_LIST);
        sessionService.add(USER2_SESSION_LIST);
        Assert.assertNotNull(sessionService.findOneById(USER1_SESSION2.getId()));
        Assert.assertNull(sessionService.removeOneById(USER2.getId(), USER1_SESSION2.getId()));
        Assert.assertNotNull(sessionService.findOneById(USER1_SESSION2.getId()));
        Assert.assertNotNull(sessionService.removeOneById(USER1.getId(), USER1_SESSION2.getId()));
        Assert.assertNull(sessionService.findOneById(USER1_SESSION2.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeOneById(NULL_USER_ID, USER1_SESSION2.getId());
        thrown.expect(IdEmptyException.class);
        sessionService.removeOneById(USER2.getId(), NULL_SESSION_ID);
    }

    @Test
    public void testSessionRemoveOneByIndex() {
        sessionService.add(USER1_SESSION1);
        Assert.assertNotNull(sessionService.findOneById(USER1_SESSION1.getId()));
        Assert.assertNotNull(sessionService.removeOneByIndex(0));
        Assert.assertNull(sessionService.findOneById(USER1_SESSION1.getId()));

        thrown.expect(IndexIncorrectException.class);
        sessionService.removeOneByIndex(-1);
    }

    @Test
    public void testSessionRemoveOneByIndexByUserId() {
        sessionService.add(USER1_SESSION1);
        sessionService.add(USER2_SESSION1);
        Assert.assertNotNull(sessionService.findOneById(USER1_SESSION1.getId()));
        Assert.assertNotNull(sessionService.findOneById(USER2_SESSION1.getId()));
        Assert.assertNotNull(sessionService.removeOneByIndex(USER1.getId(), 0));
        Assert.assertNotNull(sessionService.findOneById(USER2_SESSION1.getId()));
        Assert.assertNull(sessionService.findOneById(USER1_SESSION1.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        sessionService.removeOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testSessionRemoveAll() {
        sessionService.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), sessionService.findAll().size());
        sessionService.removeAll();
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void testSessionRemoveAllByUserId() {
        sessionService.add(USER1_SESSION_LIST);
        sessionService.add(USER2_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), sessionService.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_SESSION_LIST.size(), sessionService.findAll(USER2.getId()).size());
        sessionService.removeAll(USER1.getId());
        Assert.assertEquals(0, sessionService.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_SESSION_LIST.size(), sessionService.findAll(USER2.getId()).size());

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeAll(NULL_USER_ID);
    }

    @Test
    public void testSessionGetSize() {
        sessionService.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), sessionService.getSize());
    }

    @Test
    public void testSessionGetSizeByUserId() {
        sessionService.add(USER1_SESSION_LIST);
        sessionService.add(USER2_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), sessionService.getSize(USER1.getId()));
        Assert.assertEquals(USER2_SESSION_LIST.size(), sessionService.getSize(USER2.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.getSize(NULL_USER_ID);
    }

    @Test
    public void testSessionExistsById() {
        sessionService.add(USER1_SESSION_LIST);
        Assert.assertTrue(sessionService.existsById(USER1_SESSION1.getId()));
        Assert.assertFalse(sessionService.existsById(USER2_SESSION1.getId()));
    }

    @Test
    public void testSessionExistsByIdByUserId() {
        sessionService.add(USER1_SESSION_LIST);
        sessionService.add(USER2_SESSION_LIST);
        Assert.assertTrue(sessionService.existsById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertFalse(sessionService.existsById(USER2.getId(), USER1_SESSION1.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.existsById(NULL_USER_ID, USER1_SESSION1.getId());
    }

}
