package ru.t1.sukhorukova.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskCompleteByIndexResponse extends AbstractTaskResponse {

    public TaskCompleteByIndexResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
